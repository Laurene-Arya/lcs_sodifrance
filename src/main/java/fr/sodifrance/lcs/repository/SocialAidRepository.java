package fr.sodifrance.lcs.repository;

import fr.sodifrance.lcs.domain.SocialAid;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the SocialAid entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SocialAidRepository extends JpaRepository<SocialAid, Long> {

}
