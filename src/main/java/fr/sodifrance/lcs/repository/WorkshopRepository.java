package fr.sodifrance.lcs.repository;

import fr.sodifrance.lcs.domain.Workshop;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Workshop entity.
 */
@SuppressWarnings("unused")
@Repository
public interface WorkshopRepository extends JpaRepository<Workshop, Long> {

}
