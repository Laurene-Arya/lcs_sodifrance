package fr.sodifrance.lcs.service.impl;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.sodifrance.lcs.domain.SocialAid;
import fr.sodifrance.lcs.repository.SocialAidRepository;
import fr.sodifrance.lcs.service.SocialAidService;
import fr.sodifrance.lcs.service.dto.SocialAidDTO;
import fr.sodifrance.lcs.service.mapper.SocialAidMapper;

/**
 * Service Implementation for managing {@link SocialAid}.
 */
@Service
@Transactional
public class SocialAidServiceImpl implements SocialAidService {

    private final Logger log = LoggerFactory.getLogger(SocialAidServiceImpl.class);

    private final SocialAidRepository socialAidRepository;

    private final SocialAidMapper socialAidMapper;

    public SocialAidServiceImpl(SocialAidRepository socialAidRepository, SocialAidMapper socialAidMapper) {
        this.socialAidRepository = socialAidRepository;
        this.socialAidMapper = socialAidMapper;
    }

    /**
     * Save a socialAid.
     *
     * @param socialAidDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public SocialAidDTO save(SocialAidDTO socialAidDTO) {
        log.debug("Request to save SocialAid : {}", socialAidDTO);
        SocialAid socialAid = socialAidMapper.toEntity(socialAidDTO);
        socialAid = socialAidRepository.save(socialAid);
        SocialAidDTO result = socialAidMapper.toDto(socialAid);
        return result;
    }

    /**
     * Get all the socialAids.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<SocialAidDTO> findAll(Pageable pageable) {
        log.debug("Request to get all SocialAids");
        return socialAidRepository.findAll(pageable)
            .map(socialAidMapper::toDto);
    }


    /**
     * Get one socialAid by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<SocialAidDTO> findOne(Long id) {
        log.debug("Request to get SocialAid : {}", id);
        return socialAidRepository.findById(id)
            .map(socialAidMapper::toDto);
    }

    /**
     * Delete the socialAid by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete SocialAid : {}", id);
        socialAidRepository.deleteById(id);
    }
}
