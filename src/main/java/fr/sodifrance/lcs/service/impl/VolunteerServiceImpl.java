package fr.sodifrance.lcs.service.impl;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.sodifrance.lcs.domain.Volunteer;
import fr.sodifrance.lcs.repository.VolunteerRepository;
import fr.sodifrance.lcs.service.VolunteerService;
import fr.sodifrance.lcs.service.dto.VolunteerDTO;
import fr.sodifrance.lcs.service.mapper.VolunteerMapper;

/**
 * Service Implementation for managing {@link Volunteer}.
 */
@Service
@Transactional
public class VolunteerServiceImpl implements VolunteerService {

    private final Logger log = LoggerFactory.getLogger(VolunteerServiceImpl.class);

    private final VolunteerRepository volunteerRepository;

    private final VolunteerMapper volunteerMapper;

    public VolunteerServiceImpl(VolunteerRepository volunteerRepository, VolunteerMapper volunteerMapper) {
        this.volunteerRepository = volunteerRepository;
        this.volunteerMapper = volunteerMapper;
    }

    /**
     * Save a volunteer.
     *
     * @param volunteerDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public VolunteerDTO save(VolunteerDTO volunteerDTO) {
        log.debug("Request to save Volunteer : {}", volunteerDTO);
        Volunteer volunteer = volunteerMapper.toEntity(volunteerDTO);
        volunteer = volunteerRepository.save(volunteer);
        VolunteerDTO result = volunteerMapper.toDto(volunteer);
        return result;
    }

    /**
     * Get all the volunteers.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<VolunteerDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Volunteers");
        return volunteerRepository.findAll(pageable)
            .map(volunteerMapper::toDto);
    }


    /**
     * Get one volunteer by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<VolunteerDTO> findOne(Long id) {
        log.debug("Request to get Volunteer : {}", id);
        return volunteerRepository.findById(id)
            .map(volunteerMapper::toDto);
    }

    /**
     * Delete the volunteer by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Volunteer : {}", id);
        volunteerRepository.deleteById(id);
    }
}
