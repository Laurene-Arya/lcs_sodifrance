package fr.sodifrance.lcs.service.impl;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.sodifrance.lcs.domain.Organism;
import fr.sodifrance.lcs.repository.OrganismRepository;
import fr.sodifrance.lcs.service.OrganismService;
import fr.sodifrance.lcs.service.dto.OrganismDTO;
import fr.sodifrance.lcs.service.mapper.OrganismMapper;

/**
 * Service Implementation for managing {@link Organism}.
 */
@Service
@Transactional
public class OrganismServiceImpl implements OrganismService {

    private final Logger log = LoggerFactory.getLogger(OrganismServiceImpl.class);

    private final OrganismRepository organismRepository;

    private final OrganismMapper organismMapper;

    public OrganismServiceImpl(OrganismRepository organismRepository, OrganismMapper organismMapper) {
        this.organismRepository = organismRepository;
        this.organismMapper = organismMapper;
    }

    /**
     * Save a organism.
     *
     * @param organismDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public OrganismDTO save(OrganismDTO organismDTO) {
        log.debug("Request to save Organism : {}", organismDTO);
        Organism organism = organismMapper.toEntity(organismDTO);
        organism = organismRepository.save(organism);
        OrganismDTO result = organismMapper.toDto(organism);
        return result;
    }

    /**
     * Get all the organisms.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<OrganismDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Organisms");
        return organismRepository.findAll(pageable)
            .map(organismMapper::toDto);
    }


    /**
     * Get one organism by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<OrganismDTO> findOne(Long id) {
        log.debug("Request to get Organism : {}", id);
        return organismRepository.findById(id)
            .map(organismMapper::toDto);
    }

    /**
     * Delete the organism by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Organism : {}", id);
        organismRepository.deleteById(id);
    }
}
