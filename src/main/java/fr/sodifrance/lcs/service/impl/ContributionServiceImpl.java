package fr.sodifrance.lcs.service.impl;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.sodifrance.lcs.domain.Contribution;
import fr.sodifrance.lcs.repository.ContributionRepository;
import fr.sodifrance.lcs.service.ContributionService;
import fr.sodifrance.lcs.service.dto.ContributionDTO;
import fr.sodifrance.lcs.service.mapper.ContributionMapper;

/**
 * Service Implementation for managing {@link Contribution}.
 */
@Service
@Transactional
public class ContributionServiceImpl implements ContributionService {

    private final Logger log = LoggerFactory.getLogger(ContributionServiceImpl.class);

    private final ContributionRepository contributionRepository;

    private final ContributionMapper contributionMapper;

    public ContributionServiceImpl(ContributionRepository contributionRepository, ContributionMapper contributionMapper) {
        this.contributionRepository = contributionRepository;
        this.contributionMapper = contributionMapper;
    }

    /**
     * Save a contribution.
     *
     * @param contributionDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public ContributionDTO save(ContributionDTO contributionDTO) {
        log.debug("Request to save Contribution : {}", contributionDTO);
        Contribution contribution = contributionMapper.toEntity(contributionDTO);
        contribution = contributionRepository.save(contribution);
        ContributionDTO result = contributionMapper.toDto(contribution);
        return result;
    }

    /**
     * Get all the contributions.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ContributionDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Contributions");
        return contributionRepository.findAll(pageable)
            .map(contributionMapper::toDto);
    }


    /**
     * Get one contribution by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ContributionDTO> findOne(Long id) {
        log.debug("Request to get Contribution : {}", id);
        return contributionRepository.findById(id)
            .map(contributionMapper::toDto);
    }

    /**
     * Delete the contribution by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Contribution : {}", id);
        contributionRepository.deleteById(id);
    }
}
