package fr.sodifrance.lcs.service;

import fr.sodifrance.lcs.service.dto.VolunteerDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link fr.sodifrance.lcs.domain.Volunteer}.
 */
public interface VolunteerService {

    /**
     * Save a volunteer.
     *
     * @param volunteerDTO the entity to save.
     * @return the persisted entity.
     */
    VolunteerDTO save(VolunteerDTO volunteerDTO);

    /**
     * Get all the volunteers.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<VolunteerDTO> findAll(Pageable pageable);


    /**
     * Get the "id" volunteer.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<VolunteerDTO> findOne(Long id);

    /**
     * Delete the "id" volunteer.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
