package fr.sodifrance.lcs.service.dto;
import java.time.Instant;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link fr.sodifrance.lcs.domain.Contribution} entity.
 */
public class ContributionDTO implements Serializable {

    private Long id;

    private Instant date;

    private Long hours;

    private String total;

    private Long totalCharged;


    private Long volunteerId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getDate() {
        return date;
    }

    public void setDate(Instant date) {
        this.date = date;
    }

    public Long getHours() {
        return hours;
    }

    public void setHours(Long hours) {
        this.hours = hours;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public Long getTotalCharged() {
        return totalCharged;
    }

    public void setTotalCharged(Long totalCharged) {
        this.totalCharged = totalCharged;
    }

    public Long getVolunteerId() {
        return volunteerId;
    }

    public void setVolunteerId(Long volunteerId) {
        this.volunteerId = volunteerId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ContributionDTO contributionDTO = (ContributionDTO) o;
        if (contributionDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), contributionDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ContributionDTO{" +
            "id=" + getId() +
            ", date='" + getDate() + "'" +
            ", hours=" + getHours() +
            ", total='" + getTotal() + "'" +
            ", totalCharged=" + getTotalCharged() +
            ", volunteer=" + getVolunteerId() +
            "}";
    }
}
