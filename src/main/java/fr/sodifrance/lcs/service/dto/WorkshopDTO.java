package fr.sodifrance.lcs.service.dto;
import java.time.LocalDate;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link fr.sodifrance.lcs.domain.Workshop} entity.
 */
public class WorkshopDTO implements Serializable {

    private Long id;

    private LocalDate date;

    private String periode;


    private Long beneficiaryId;

    private Long organismId;

    private Long volunteerId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public String getPeriode() {
        return periode;
    }

    public void setPeriode(String periode) {
        this.periode = periode;
    }

    public Long getBeneficiaryId() {
        return beneficiaryId;
    }

    public void setBeneficiaryId(Long beneficiaryId) {
        this.beneficiaryId = beneficiaryId;
    }

    public Long getOrganismId() {
        return organismId;
    }

    public void setOrganismId(Long organismId) {
        this.organismId = organismId;
    }

    public Long getVolunteerId() {
        return volunteerId;
    }

    public void setVolunteerId(Long volunteerId) {
        this.volunteerId = volunteerId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        WorkshopDTO workshopDTO = (WorkshopDTO) o;
        if (workshopDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), workshopDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "WorkshopDTO{" +
            "id=" + getId() +
            ", date='" + getDate() + "'" +
            ", periode='" + getPeriode() + "'" +
            ", beneficiary=" + getBeneficiaryId() +
            ", organism=" + getOrganismId() +
            ", volunteer=" + getVolunteerId() +
            "}";
    }
}
