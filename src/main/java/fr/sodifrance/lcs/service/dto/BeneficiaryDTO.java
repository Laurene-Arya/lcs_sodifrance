package fr.sodifrance.lcs.service.dto;
import java.time.LocalDate;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link fr.sodifrance.lcs.domain.Beneficiary} entity.
 */
public class BeneficiaryDTO implements Serializable {

    private Long id;

    private String lastName;

    private String firstName;

    private String email;

    private String phoneNumber;

    private String gender;

    private LocalDate birthDate;

    private String Address;

    private Long CP;

    private String City;

    private Boolean hasCV;

    private String financialResources;

    private String levelOfStudy;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String Address) {
        this.Address = Address;
    }

    public Long getCP() {
        return CP;
    }

    public void setCP(Long CP) {
        this.CP = CP;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String City) {
        this.City = City;
    }

    public Boolean isHasCV() {
        return hasCV;
    }

    public void setHasCV(Boolean hasCV) {
        this.hasCV = hasCV;
    }

    public String getFinancialResources() {
        return financialResources;
    }

    public void setFinancialResources(String financialResources) {
        this.financialResources = financialResources;
    }

    public String getLevelOfStudy() {
        return levelOfStudy;
    }

    public void setLevelOfStudy(String levelOfStudy) {
        this.levelOfStudy = levelOfStudy;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        BeneficiaryDTO beneficiaryDTO = (BeneficiaryDTO) o;
        if (beneficiaryDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), beneficiaryDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "BeneficiaryDTO{" +
            "id=" + getId() +
            ", lastName='" + getLastName() + "'" +
            ", firstName='" + getFirstName() + "'" +
            ", email='" + getEmail() + "'" +
            ", phoneNumber='" + getPhoneNumber() + "'" +
            ", gender='" + getGender() + "'" +
            ", birthDate='" + getBirthDate() + "'" +
            ", Address='" + getAddress() + "'" +
            ", CP=" + getCP() +
            ", City='" + getCity() + "'" +
            ", hasCV='" + isHasCV() + "'" +
            ", financialResources='" + getFinancialResources() + "'" +
            ", levelOfStudy='" + getLevelOfStudy() + "'" +
            "}";
    }
}
