package fr.sodifrance.lcs.service.mapper;

import fr.sodifrance.lcs.domain.*;
import fr.sodifrance.lcs.service.dto.OrganismDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Organism} and its DTO {@link OrganismDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface OrganismMapper extends EntityMapper<OrganismDTO, Organism> {



    default Organism fromId(Long id) {
        if (id == null) {
            return null;
        }
        Organism organism = new Organism();
        organism.setId(id);
        return organism;
    }
}
