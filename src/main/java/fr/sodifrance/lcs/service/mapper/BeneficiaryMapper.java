package fr.sodifrance.lcs.service.mapper;

import fr.sodifrance.lcs.domain.*;
import fr.sodifrance.lcs.service.dto.BeneficiaryDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Beneficiary} and its DTO {@link BeneficiaryDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface BeneficiaryMapper extends EntityMapper<BeneficiaryDTO, Beneficiary> {


    @Mapping(target = "workshops", ignore = true)
    @Mapping(target = "removeWorkshops", ignore = true)
    @Mapping(target = "socialAids", ignore = true)
    @Mapping(target = "removeSocialAids", ignore = true)
    Beneficiary toEntity(BeneficiaryDTO beneficiaryDTO);

    default Beneficiary fromId(Long id) {
        if (id == null) {
            return null;
        }
        Beneficiary beneficiary = new Beneficiary();
        beneficiary.setId(id);
        return beneficiary;
    }
}
