package fr.sodifrance.lcs.service.mapper;

import fr.sodifrance.lcs.domain.*;
import fr.sodifrance.lcs.service.dto.SocialAidDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link SocialAid} and its DTO {@link SocialAidDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface SocialAidMapper extends EntityMapper<SocialAidDTO, SocialAid> {



    default SocialAid fromId(Long id) {
        if (id == null) {
            return null;
        }
        SocialAid socialAid = new SocialAid();
        socialAid.setId(id);
        return socialAid;
    }
}
