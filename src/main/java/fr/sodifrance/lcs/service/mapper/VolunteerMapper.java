package fr.sodifrance.lcs.service.mapper;

import fr.sodifrance.lcs.domain.*;
import fr.sodifrance.lcs.service.dto.VolunteerDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Volunteer} and its DTO {@link VolunteerDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface VolunteerMapper extends EntityMapper<VolunteerDTO, Volunteer> {


    @Mapping(target = "workshops", ignore = true)
    @Mapping(target = "removeWorkshops", ignore = true)
    @Mapping(target = "skills", ignore = true)
    @Mapping(target = "removeSkills", ignore = true)
    @Mapping(target = "constributions", ignore = true)
    @Mapping(target = "removeConstributions", ignore = true)
    Volunteer toEntity(VolunteerDTO volunteerDTO);

    default Volunteer fromId(Long id) {
        if (id == null) {
            return null;
        }
        Volunteer volunteer = new Volunteer();
        volunteer.setId(id);
        return volunteer;
    }
}
