package fr.sodifrance.lcs.service.mapper;

import fr.sodifrance.lcs.domain.*;
import fr.sodifrance.lcs.service.dto.ContributionDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Contribution} and its DTO {@link ContributionDTO}.
 */
@Mapper(componentModel = "spring", uses = {VolunteerMapper.class})
public interface ContributionMapper extends EntityMapper<ContributionDTO, Contribution> {

    @Mapping(source = "volunteer.id", target = "volunteerId")
    ContributionDTO toDto(Contribution contribution);

    @Mapping(source = "volunteerId", target = "volunteer")
    Contribution toEntity(ContributionDTO contributionDTO);

    default Contribution fromId(Long id) {
        if (id == null) {
            return null;
        }
        Contribution contribution = new Contribution();
        contribution.setId(id);
        return contribution;
    }
}
