package fr.sodifrance.lcs.service.mapper;

import fr.sodifrance.lcs.domain.*;
import fr.sodifrance.lcs.service.dto.WorkshopDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Workshop} and its DTO {@link WorkshopDTO}.
 */
@Mapper(componentModel = "spring", uses = {BeneficiaryMapper.class, OrganismMapper.class, VolunteerMapper.class})
public interface WorkshopMapper extends EntityMapper<WorkshopDTO, Workshop> {

    @Mapping(source = "beneficiary.id", target = "beneficiaryId")
    @Mapping(source = "organism.id", target = "organismId")
    @Mapping(source = "volunteer.id", target = "volunteerId")
    WorkshopDTO toDto(Workshop workshop);

    @Mapping(source = "beneficiaryId", target = "beneficiary")
    @Mapping(source = "organismId", target = "organism")
    @Mapping(source = "volunteerId", target = "volunteer")
    Workshop toEntity(WorkshopDTO workshopDTO);

    default Workshop fromId(Long id) {
        if (id == null) {
            return null;
        }
        Workshop workshop = new Workshop();
        workshop.setId(id);
        return workshop;
    }
}
