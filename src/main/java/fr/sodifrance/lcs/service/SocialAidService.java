package fr.sodifrance.lcs.service;

import fr.sodifrance.lcs.service.dto.SocialAidDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link fr.sodifrance.lcs.domain.SocialAid}.
 */
public interface SocialAidService {

    /**
     * Save a socialAid.
     *
     * @param socialAidDTO the entity to save.
     * @return the persisted entity.
     */
    SocialAidDTO save(SocialAidDTO socialAidDTO);

    /**
     * Get all the socialAids.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<SocialAidDTO> findAll(Pageable pageable);


    /**
     * Get the "id" socialAid.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<SocialAidDTO> findOne(Long id);

    /**
     * Delete the "id" socialAid.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
