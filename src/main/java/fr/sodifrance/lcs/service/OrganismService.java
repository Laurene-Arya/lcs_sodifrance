package fr.sodifrance.lcs.service;

import fr.sodifrance.lcs.service.dto.OrganismDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link fr.sodifrance.lcs.domain.Organism}.
 */
public interface OrganismService {

    /**
     * Save a organism.
     *
     * @param organismDTO the entity to save.
     * @return the persisted entity.
     */
    OrganismDTO save(OrganismDTO organismDTO);

    /**
     * Get all the organisms.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<OrganismDTO> findAll(Pageable pageable);


    /**
     * Get the "id" organism.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<OrganismDTO> findOne(Long id);

    /**
     * Delete the "id" organism.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
