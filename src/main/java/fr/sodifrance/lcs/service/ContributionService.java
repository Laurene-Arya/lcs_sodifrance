package fr.sodifrance.lcs.service;

import fr.sodifrance.lcs.service.dto.ContributionDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link fr.sodifrance.lcs.domain.Contribution}.
 */
public interface ContributionService {

    /**
     * Save a contribution.
     *
     * @param contributionDTO the entity to save.
     * @return the persisted entity.
     */
    ContributionDTO save(ContributionDTO contributionDTO);

    /**
     * Get all the contributions.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ContributionDTO> findAll(Pageable pageable);


    /**
     * Get the "id" contribution.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ContributionDTO> findOne(Long id);

    /**
     * Delete the "id" contribution.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
