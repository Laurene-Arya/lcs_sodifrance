package fr.sodifrance.lcs.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import fr.sodifrance.lcs.service.OrganismService;
import fr.sodifrance.lcs.service.dto.OrganismDTO;
import fr.sodifrance.lcs.web.rest.errors.BadRequestAlertException;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link fr.sodifrance.lcs.domain.Organism}.
 */
@RestController
@RequestMapping("/api")
public class OrganismResource {

    private final Logger log = LoggerFactory.getLogger(OrganismResource.class);

    private static final String ENTITY_NAME = "organism";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final OrganismService organismService;

    public OrganismResource(OrganismService organismService) {
        this.organismService = organismService;
    }

    /**
     * {@code POST  /organisms} : Create a new organism.
     *
     * @param organismDTO the organismDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new organismDTO, or with status {@code 400 (Bad Request)} if the organism has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/organisms")
    public ResponseEntity<OrganismDTO> createOrganism(@RequestBody OrganismDTO organismDTO) throws URISyntaxException {
        log.debug("REST request to save Organism : {}", organismDTO);
        if (organismDTO.getId() != null) {
            throw new BadRequestAlertException("A new organism cannot already have an ID", ENTITY_NAME, "idexists");
        }
        OrganismDTO result = organismService.save(organismDTO);
        return ResponseEntity.created(new URI("/api/organisms/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /organisms} : Updates an existing organism.
     *
     * @param organismDTO the organismDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated organismDTO,
     * or with status {@code 400 (Bad Request)} if the organismDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the organismDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/organisms")
    public ResponseEntity<OrganismDTO> updateOrganism(@RequestBody OrganismDTO organismDTO) throws URISyntaxException {
        log.debug("REST request to update Organism : {}", organismDTO);
        if (organismDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        OrganismDTO result = organismService.save(organismDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, organismDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /organisms} : get all the organisms.
     *

     * @param pageable the pagination information.

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of organisms in body.
     */
    @GetMapping("/organisms")
    public ResponseEntity<List<OrganismDTO>> getAllOrganisms(Pageable pageable) {
        log.debug("REST request to get a page of Organisms");
        Page<OrganismDTO> page = organismService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /organisms/:id} : get the "id" organism.
     *
     * @param id the id of the organismDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the organismDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/organisms/{id}")
    public ResponseEntity<OrganismDTO> getOrganism(@PathVariable Long id) {
        log.debug("REST request to get Organism : {}", id);
        Optional<OrganismDTO> organismDTO = organismService.findOne(id);
        return ResponseUtil.wrapOrNotFound(organismDTO);
    }

    /**
     * {@code DELETE  /organisms/:id} : delete the "id" organism.
     *
     * @param id the id of the organismDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/organisms/{id}")
    public ResponseEntity<Void> deleteOrganism(@PathVariable Long id) {
        log.debug("REST request to delete Organism : {}", id);
        organismService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }

}
