package fr.sodifrance.lcs.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import fr.sodifrance.lcs.service.SocialAidService;
import fr.sodifrance.lcs.service.dto.SocialAidDTO;
import fr.sodifrance.lcs.web.rest.errors.BadRequestAlertException;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link fr.sodifrance.lcs.domain.SocialAid}.
 */
@RestController
@RequestMapping("/api")
public class SocialAidResource {

    private final Logger log = LoggerFactory.getLogger(SocialAidResource.class);

    private static final String ENTITY_NAME = "socialAid";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final SocialAidService socialAidService;

    public SocialAidResource(SocialAidService socialAidService) {
        this.socialAidService = socialAidService;
    }

    /**
     * {@code POST  /social-aids} : Create a new socialAid.
     *
     * @param socialAidDTO the socialAidDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new socialAidDTO, or with status {@code 400 (Bad Request)} if the socialAid has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/social-aids")
    public ResponseEntity<SocialAidDTO> createSocialAid(@RequestBody SocialAidDTO socialAidDTO) throws URISyntaxException {
        log.debug("REST request to save SocialAid : {}", socialAidDTO);
        if (socialAidDTO.getId() != null) {
            throw new BadRequestAlertException("A new socialAid cannot already have an ID", ENTITY_NAME, "idexists");
        }
        SocialAidDTO result = socialAidService.save(socialAidDTO);
        return ResponseEntity.created(new URI("/api/social-aids/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /social-aids} : Updates an existing socialAid.
     *
     * @param socialAidDTO the socialAidDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated socialAidDTO,
     * or with status {@code 400 (Bad Request)} if the socialAidDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the socialAidDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/social-aids")
    public ResponseEntity<SocialAidDTO> updateSocialAid(@RequestBody SocialAidDTO socialAidDTO) throws URISyntaxException {
        log.debug("REST request to update SocialAid : {}", socialAidDTO);
        if (socialAidDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        SocialAidDTO result = socialAidService.save(socialAidDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, socialAidDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /social-aids} : get all the socialAids.
     *

     * @param pageable the pagination information.

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of socialAids in body.
     */
    @GetMapping("/social-aids")
    public ResponseEntity<List<SocialAidDTO>> getAllSocialAids(Pageable pageable) {
        log.debug("REST request to get a page of SocialAids");
        Page<SocialAidDTO> page = socialAidService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /social-aids/:id} : get the "id" socialAid.
     *
     * @param id the id of the socialAidDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the socialAidDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/social-aids/{id}")
    public ResponseEntity<SocialAidDTO> getSocialAid(@PathVariable Long id) {
        log.debug("REST request to get SocialAid : {}", id);
        Optional<SocialAidDTO> socialAidDTO = socialAidService.findOne(id);
        return ResponseUtil.wrapOrNotFound(socialAidDTO);
    }

    /**
     * {@code DELETE  /social-aids/:id} : delete the "id" socialAid.
     *
     * @param id the id of the socialAidDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/social-aids/{id}")
    public ResponseEntity<Void> deleteSocialAid(@PathVariable Long id) {
        log.debug("REST request to delete SocialAid : {}", id);
        socialAidService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }

}
