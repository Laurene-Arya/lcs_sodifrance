/**
 * View Models used by Spring MVC REST controllers.
 */
package fr.sodifrance.lcs.web.rest.vm;
