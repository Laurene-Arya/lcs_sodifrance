package fr.sodifrance.lcs.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import fr.sodifrance.lcs.service.WorkshopService;
import fr.sodifrance.lcs.service.dto.WorkshopDTO;
import fr.sodifrance.lcs.web.rest.errors.BadRequestAlertException;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link fr.sodifrance.lcs.domain.Workshop}.
 */
@RestController
@RequestMapping("/api")
public class WorkshopResource {

    private final Logger log = LoggerFactory.getLogger(WorkshopResource.class);

    private static final String ENTITY_NAME = "workshop";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final WorkshopService workshopService;

    public WorkshopResource(WorkshopService workshopService) {
        this.workshopService = workshopService;
    }

    /**
     * {@code POST  /workshops} : Create a new workshop.
     *
     * @param workshopDTO the workshopDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new workshopDTO, or with status {@code 400 (Bad Request)} if the workshop has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/workshops")
    public ResponseEntity<WorkshopDTO> createWorkshop(@RequestBody WorkshopDTO workshopDTO) throws URISyntaxException {
        log.debug("REST request to save Workshop : {}", workshopDTO);
        if (workshopDTO.getId() != null) {
            throw new BadRequestAlertException("A new workshop cannot already have an ID", ENTITY_NAME, "idexists");
        }
        WorkshopDTO result = workshopService.save(workshopDTO);
        return ResponseEntity.created(new URI("/api/workshops/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /workshops} : Updates an existing workshop.
     *
     * @param workshopDTO the workshopDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated workshopDTO,
     * or with status {@code 400 (Bad Request)} if the workshopDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the workshopDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/workshops")
    public ResponseEntity<WorkshopDTO> updateWorkshop(@RequestBody WorkshopDTO workshopDTO) throws URISyntaxException {
        log.debug("REST request to update Workshop : {}", workshopDTO);
        if (workshopDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        WorkshopDTO result = workshopService.save(workshopDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, workshopDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /workshops} : get all the workshops.
     *

     * @param pageable the pagination information.

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of workshops in body.
     */
    @GetMapping("/workshops")
    public ResponseEntity<List<WorkshopDTO>> getAllWorkshops(Pageable pageable) {
        log.debug("REST request to get a page of Workshops");
        Page<WorkshopDTO> page = workshopService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /workshops/:id} : get the "id" workshop.
     *
     * @param id the id of the workshopDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the workshopDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/workshops/{id}")
    public ResponseEntity<WorkshopDTO> getWorkshop(@PathVariable Long id) {
        log.debug("REST request to get Workshop : {}", id);
        Optional<WorkshopDTO> workshopDTO = workshopService.findOne(id);
        return ResponseUtil.wrapOrNotFound(workshopDTO);
    }

    /**
     * {@code DELETE  /workshops/:id} : delete the "id" workshop.
     *
     * @param id the id of the workshopDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/workshops/{id}")
    public ResponseEntity<Void> deleteWorkshop(@PathVariable Long id) {
        log.debug("REST request to delete Workshop : {}", id);
        workshopService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
