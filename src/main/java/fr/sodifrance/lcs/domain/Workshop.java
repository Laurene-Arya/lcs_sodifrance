package fr.sodifrance.lcs.domain;
import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Workshop.
 */
@Entity
@Table(name = "workshop")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Workshop implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "date")
    private LocalDate date;

    @Column(name = "periode")
    private String periode;

    @OneToOne
    @JoinColumn(unique = true)
    private Beneficiary beneficiary;

    @OneToOne
    @JoinColumn(unique = true)
    private Organism organism;

    @OneToOne
    @JoinColumn(unique = true)
    private Volunteer volunteer;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDate() {
        return date;
    }

    public Workshop date(LocalDate date) {
        this.date = date;
        return this;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public String getPeriode() {
        return periode;
    }

    public Workshop periode(String periode) {
        this.periode = periode;
        return this;
    }

    public void setPeriode(String periode) {
        this.periode = periode;
    }

    public Beneficiary getBeneficiary() {
        return beneficiary;
    }

    public Workshop beneficiary(Beneficiary beneficiary) {
        this.beneficiary = beneficiary;
        return this;
    }

    public void setBeneficiary(Beneficiary beneficiary) {
        this.beneficiary = beneficiary;
    }

    public Organism getOrganism() {
        return organism;
    }

    public Workshop organism(Organism organism) {
        this.organism = organism;
        return this;
    }

    public void setOrganism(Organism organism) {
        this.organism = organism;
    }
    
    public Volunteer getVolunteer() {
        return volunteer;
    }

    public Workshop volunteer(Volunteer volunteer) {
        this.volunteer = volunteer;
        return this;
    }

    public void setVolunteer(Volunteer volunteer) {
        this.volunteer = volunteer;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Workshop)) {
            return false;
        }
        return id != null && id.equals(((Workshop) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Workshop{" +
            "id=" + getId() +
            ", date='" + getDate() + "'" +
            ", periode='" + getPeriode() + "'" +
            "}";
    }
}
