package fr.sodifrance.lcs.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;

/**
 * A Contribution.
 */
@Entity
@Table(name = "contribution")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Contribution implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "date")
    private Instant date;

    @Column(name = "hours")
    private Long hours;

    @Column(name = "total")
    private String total;

    @Column(name = "total_charged")
    private Long totalCharged;

    @OneToOne
    @JoinColumn(unique = true)
    private Volunteer volunteer;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getDate() {
        return date;
    }

    public Contribution date(Instant date) {
        this.date = date;
        return this;
    }

    public void setDate(Instant date) {
        this.date = date;
    }

    public Long getHours() {
        return hours;
    }

    public Contribution hours(Long hours) {
        this.hours = hours;
        return this;
    }

    public void setHours(Long hours) {
        this.hours = hours;
    }

    public String getTotal() {
        return total;
    }

    public Contribution total(String total) {
        this.total = total;
        return this;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public Long getTotalCharged() {
        return totalCharged;
    }

    public Contribution totalCharged(Long totalCharged) {
        this.totalCharged = totalCharged;
        return this;
    }

    public void setTotalCharged(Long totalCharged) {
        this.totalCharged = totalCharged;
    }

    public Volunteer getVolunteer() {
        return volunteer;
    }

    public Contribution volunteer(Volunteer volunteer) {
        this.volunteer = volunteer;
        return this;
    }

    public void setVolunteer(Volunteer volunteer) {
        this.volunteer = volunteer;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Contribution)) {
            return false;
        }
        return id != null && id.equals(((Contribution) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Contribution{" +
            "id=" + getId() +
            ", date='" + getDate() + "'" +
            ", hours=" + getHours() +
            ", total='" + getTotal() + "'" +
            ", totalCharged=" + getTotalCharged() +
            "}";
    }
}
