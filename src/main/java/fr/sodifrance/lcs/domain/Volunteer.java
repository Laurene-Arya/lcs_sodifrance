package fr.sodifrance.lcs.domain;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Volunteer.
 */
@Entity
@Table(name = "volunteer")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Volunteer implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @OneToMany(mappedBy = "volunteer")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Workshop> workshops = new HashSet<>();

    @OneToMany()
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Skill> skills = new HashSet<>();

    @OneToMany(mappedBy = "volunteer")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Contribution> constributions = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public Volunteer firstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Volunteer lastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Set<Workshop> getWorkshops() {
        return workshops;
    }

    public Volunteer workshops(Set<Workshop> workshops) {
        this.workshops = workshops;
        return this;
    }

    public Volunteer addWorkshops(Workshop workshop) {
        this.workshops.add(workshop);
        workshop.setVolunteer(this);
        return this;
    }

    public Volunteer removeWorkshops(Workshop workshop) {
        this.workshops.remove(workshop);
        workshop.setVolunteer(null);
        return this;
    }

    public void setWorkshops(Set<Workshop> workshops) {
        this.workshops = workshops;
    }

    public Set<Skill> getSkills() {
        return skills;
    }

    public Volunteer skills(Set<Skill> skills) {
        this.skills = skills;
        return this;
    }

    public Volunteer addSkills(Skill skill) {
        this.skills.add(skill);
        return this;
    }

    public Volunteer removeSkills(Skill skill) {
        this.skills.remove(skill);
        return this;
    }

    public void setSkills(Set<Skill> skills) {
        this.skills = skills;
    }

    public Set<Contribution> getConstributions() {
        return constributions;
    }

    public Volunteer constributions(Set<Contribution> contributions) {
        this.constributions = contributions;
        return this;
    }

    public Volunteer addConstributions(Contribution contribution) {
        this.constributions.add(contribution);
        contribution.setVolunteer(this);
        return this;
    }

    public Volunteer removeConstributions(Contribution contribution) {
        this.constributions.remove(contribution);
        contribution.setVolunteer(null);
        return this;
    }

    public void setConstributions(Set<Contribution> contributions) {
        this.constributions = contributions;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Volunteer)) {
            return false;
        }
        return id != null && id.equals(((Volunteer) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Volunteer{" +
            "id=" + getId() +
            ", firstName='" + getFirstName() + "'" +
            ", lastName='" + getLastName() + "'" +
            "}";
    }
}
