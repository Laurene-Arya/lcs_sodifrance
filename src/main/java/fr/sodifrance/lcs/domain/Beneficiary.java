package fr.sodifrance.lcs.domain;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Beneficiary.
 */
@Entity
@Table(name = "beneficiary")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Beneficiary implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "email")
    private String email;

    @Column(name = "phone_number")
    private String phoneNumber;

    @Column(name = "gender")
    private String gender;

    @Column(name = "birth_date")
    private LocalDate birthDate;

    @Column(name = "address")
    private String Address;

    @Column(name = "cp")
    private Long CP;

    @Column(name = "city")
    private String City;

    @Column(name = "has_cv")
    private Boolean hasCV;

    @Column(name = "financial_resources")
    private String financialResources;

    @Column(name = "level_of_study")
    private String levelOfStudy;

    @OneToMany(mappedBy = "beneficiary")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Workshop> workshops = new HashSet<>();

    @OneToMany()
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<SocialAid> socialAids = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLastName() {
        return lastName;
    }

    public Beneficiary lastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public Beneficiary firstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getEmail() {
        return email;
    }

    public Beneficiary email(String email) {
        this.email = email;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public Beneficiary phoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
        return this;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getGender() {
        return gender;
    }

    public Beneficiary gender(String gender) {
        this.gender = gender;
        return this;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public Beneficiary birthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
        return this;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public String getAddress() {
        return Address;
    }

    public Beneficiary Address(String Address) {
        this.Address = Address;
        return this;
    }

    public void setAddress(String Address) {
        this.Address = Address;
    }

    public Long getCP() {
        return CP;
    }

    public Beneficiary CP(Long CP) {
        this.CP = CP;
        return this;
    }

    public void setCP(Long CP) {
        this.CP = CP;
    }

    public String getCity() {
        return City;
    }

    public Beneficiary City(String City) {
        this.City = City;
        return this;
    }

    public void setCity(String City) {
        this.City = City;
    }

    public Boolean isHasCV() {
        return hasCV;
    }

    public Beneficiary hasCV(Boolean hasCV) {
        this.hasCV = hasCV;
        return this;
    }

    public void setHasCV(Boolean hasCV) {
        this.hasCV = hasCV;
    }

    public String getFinancialResources() {
        return financialResources;
    }

    public Beneficiary financialResources(String financialResources) {
        this.financialResources = financialResources;
        return this;
    }

    public void setFinancialResources(String financialResources) {
        this.financialResources = financialResources;
    }

    public String getLevelOfStudy() {
        return levelOfStudy;
    }

    public Beneficiary levelOfStudy(String levelOfStudy) {
        this.levelOfStudy = levelOfStudy;
        return this;
    }

    public void setLevelOfStudy(String levelOfStudy) {
        this.levelOfStudy = levelOfStudy;
    }

    public Set<Workshop> getWorkshops() {
        return workshops;
    }

    public Beneficiary workshops(Set<Workshop> workshops) {
        this.workshops = workshops;
        return this;
    }

    public Beneficiary addWorkshops(Workshop workshop) {
        this.workshops.add(workshop);
        workshop.setBeneficiary(this);
        return this;
    }

    public Beneficiary removeWorkshops(Workshop workshop) {
        this.workshops.remove(workshop);
        workshop.setBeneficiary(null);
        return this;
    }

    public void setWorkshops(Set<Workshop> workshops) {
        this.workshops = workshops;
    }

    public Set<SocialAid> getSocialAids() {
        return socialAids;
    }

    public Beneficiary socialAids(Set<SocialAid> socialAids) {
        this.socialAids = socialAids;
        return this;
    }

    public Beneficiary addSocialAids(SocialAid socialAid) {
        this.socialAids.add(socialAid);
        return this;
    }

    public Beneficiary removeSocialAids(SocialAid socialAid) {
        this.socialAids.remove(socialAid);
        return this;
    }

    public void setSocialAids(Set<SocialAid> socialAids) {
        this.socialAids = socialAids;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Beneficiary)) {
            return false;
        }
        return id != null && id.equals(((Beneficiary) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Beneficiary{" +
            "id=" + getId() +
            ", lastName='" + getLastName() + "'" +
            ", firstName='" + getFirstName() + "'" +
            ", email='" + getEmail() + "'" +
            ", phoneNumber='" + getPhoneNumber() + "'" +
            ", gender='" + getGender() + "'" +
            ", birthDate='" + getBirthDate() + "'" +
            ", Address='" + getAddress() + "'" +
            ", CP=" + getCP() +
            ", City='" + getCity() + "'" +
            ", hasCV='" + isHasCV() + "'" +
            ", financialResources='" + getFinancialResources() + "'" +
            ", levelOfStudy='" + getLevelOfStudy() + "'" +
            "}";
    }
}
