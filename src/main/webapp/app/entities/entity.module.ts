import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'organism',
        loadChildren: () => import('./organism/organism.module').then(m => m.LcsOrganismModule)
      },
      {
        path: 'beneficiary',
        loadChildren: () => import('./beneficiary/beneficiary.module').then(m => m.LcsBeneficiaryModule)
      },
      {
        path: 'social-aid',
        loadChildren: () => import('./social-aid/social-aid.module').then(m => m.LcsSocialAidModule)
      },
      {
        path: 'workshop',
        loadChildren: () => import('./workshop/workshop.module').then(m => m.LcsWorkshopModule)
      },
      {
        path: 'volunteer',
        loadChildren: () => import('./volunteer/volunteer.module').then(m => m.LcsVolunteerModule)
      },
      {
        path: 'contribution',
        loadChildren: () => import('./contribution/contribution.module').then(m => m.LcsContributionModule)
      },
      {
        path: 'skill',
        loadChildren: () => import('./skill/skill.module').then(m => m.LcsSkillModule)
      }
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ])
  ],
  declarations: [],
  entryComponents: [],
  providers: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class LcsEntityModule {}
