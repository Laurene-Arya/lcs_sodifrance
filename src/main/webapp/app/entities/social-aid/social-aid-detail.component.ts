import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ISocialAid } from 'app/shared/model/social-aid.model';

@Component({
  selector: 'jhi-social-aid-detail',
  templateUrl: './social-aid-detail.component.html'
})
export class SocialAidDetailComponent implements OnInit {
  socialAid: ISocialAid;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ socialAid }) => {
      this.socialAid = socialAid;
    });
  }

  previousState() {
    window.history.back();
  }
}
