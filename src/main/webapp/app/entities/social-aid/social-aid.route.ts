import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { SocialAid } from 'app/shared/model/social-aid.model';
import { SocialAidService } from './social-aid.service';
import { SocialAidComponent } from './social-aid.component';
import { SocialAidDetailComponent } from './social-aid-detail.component';
import { SocialAidUpdateComponent } from './social-aid-update.component';
import { SocialAidDeletePopupComponent } from './social-aid-delete-dialog.component';
import { ISocialAid } from 'app/shared/model/social-aid.model';

@Injectable({ providedIn: 'root' })
export class SocialAidResolve implements Resolve<ISocialAid> {
  constructor(private service: SocialAidService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ISocialAid> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<SocialAid>) => response.ok),
        map((socialAid: HttpResponse<SocialAid>) => socialAid.body)
      );
    }
    return of(new SocialAid());
  }
}

export const socialAidRoute: Routes = [
  {
    path: '',
    component: SocialAidComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'SocialAids'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: SocialAidDetailComponent,
    resolve: {
      socialAid: SocialAidResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'SocialAids'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: SocialAidUpdateComponent,
    resolve: {
      socialAid: SocialAidResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'SocialAids'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: SocialAidUpdateComponent,
    resolve: {
      socialAid: SocialAidResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'SocialAids'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const socialAidPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: SocialAidDeletePopupComponent,
    resolve: {
      socialAid: SocialAidResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'SocialAids'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
