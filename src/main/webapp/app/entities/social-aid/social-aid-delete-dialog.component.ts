import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ISocialAid } from 'app/shared/model/social-aid.model';
import { SocialAidService } from './social-aid.service';

@Component({
  selector: 'jhi-social-aid-delete-dialog',
  templateUrl: './social-aid-delete-dialog.component.html'
})
export class SocialAidDeleteDialogComponent {
  socialAid: ISocialAid;

  constructor(protected socialAidService: SocialAidService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.socialAidService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'socialAidListModification',
        content: 'Deleted an socialAid'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-social-aid-delete-popup',
  template: ''
})
export class SocialAidDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ socialAid }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(SocialAidDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.socialAid = socialAid;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/social-aid', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/social-aid', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
