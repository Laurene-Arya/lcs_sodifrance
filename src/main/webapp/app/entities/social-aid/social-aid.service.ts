import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { ISocialAid } from 'app/shared/model/social-aid.model';

type EntityResponseType = HttpResponse<ISocialAid>;
type EntityArrayResponseType = HttpResponse<ISocialAid[]>;

@Injectable({ providedIn: 'root' })
export class SocialAidService {
  public resourceUrl = SERVER_API_URL + 'api/social-aids';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/social-aids';

  constructor(protected http: HttpClient) {}

  create(socialAid: ISocialAid): Observable<EntityResponseType> {
    return this.http.post<ISocialAid>(this.resourceUrl, socialAid, { observe: 'response' });
  }

  update(socialAid: ISocialAid): Observable<EntityResponseType> {
    return this.http.put<ISocialAid>(this.resourceUrl, socialAid, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ISocialAid>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ISocialAid[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ISocialAid[]>(this.resourceSearchUrl, { params: options, observe: 'response' });
  }
}
