import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { LcsSharedModule } from 'app/shared';
import {
  SocialAidComponent,
  SocialAidDetailComponent,
  SocialAidUpdateComponent,
  SocialAidDeletePopupComponent,
  SocialAidDeleteDialogComponent,
  socialAidRoute,
  socialAidPopupRoute
} from './';

const ENTITY_STATES = [...socialAidRoute, ...socialAidPopupRoute];

@NgModule({
  imports: [LcsSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    SocialAidComponent,
    SocialAidDetailComponent,
    SocialAidUpdateComponent,
    SocialAidDeleteDialogComponent,
    SocialAidDeletePopupComponent
  ],
  entryComponents: [SocialAidComponent, SocialAidUpdateComponent, SocialAidDeleteDialogComponent, SocialAidDeletePopupComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class LcsSocialAidModule {}
