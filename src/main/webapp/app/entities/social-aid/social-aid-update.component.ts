import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { ISocialAid, SocialAid } from 'app/shared/model/social-aid.model';
import { SocialAidService } from './social-aid.service';

@Component({
  selector: 'jhi-social-aid-update',
  templateUrl: './social-aid-update.component.html'
})
export class SocialAidUpdateComponent implements OnInit {
  isSaving: boolean;

  editForm = this.fb.group({
    id: [],
    libelle: []
  });

  constructor(protected socialAidService: SocialAidService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ socialAid }) => {
      this.updateForm(socialAid);
    });
  }

  updateForm(socialAid: ISocialAid) {
    this.editForm.patchValue({
      id: socialAid.id,
      libelle: socialAid.libelle
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const socialAid = this.createFromForm();
    if (socialAid.id !== undefined) {
      this.subscribeToSaveResponse(this.socialAidService.update(socialAid));
    } else {
      this.subscribeToSaveResponse(this.socialAidService.create(socialAid));
    }
  }

  private createFromForm(): ISocialAid {
    return {
      ...new SocialAid(),
      id: this.editForm.get(['id']).value,
      libelle: this.editForm.get(['libelle']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ISocialAid>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
}
