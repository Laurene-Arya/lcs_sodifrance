import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { LcsSharedModule } from 'app/shared';
import {
  BeneficiaryComponent,
  BeneficiaryDetailComponent,
  BeneficiaryUpdateComponent,
  BeneficiaryDeletePopupComponent,
  BeneficiaryDeleteDialogComponent,
  beneficiaryRoute,
  beneficiaryPopupRoute
} from './';

const ENTITY_STATES = [...beneficiaryRoute, ...beneficiaryPopupRoute];

@NgModule({
  imports: [LcsSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    BeneficiaryComponent,
    BeneficiaryDetailComponent,
    BeneficiaryUpdateComponent,
    BeneficiaryDeleteDialogComponent,
    BeneficiaryDeletePopupComponent
  ],
  entryComponents: [BeneficiaryComponent, BeneficiaryUpdateComponent, BeneficiaryDeleteDialogComponent, BeneficiaryDeletePopupComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class LcsBeneficiaryModule {}
