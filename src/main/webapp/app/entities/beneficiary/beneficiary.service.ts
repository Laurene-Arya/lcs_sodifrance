import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IBeneficiary } from 'app/shared/model/beneficiary.model';

type EntityResponseType = HttpResponse<IBeneficiary>;
type EntityArrayResponseType = HttpResponse<IBeneficiary[]>;

@Injectable({ providedIn: 'root' })
export class BeneficiaryService {
  public resourceUrl = SERVER_API_URL + 'api/beneficiaries';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/beneficiaries';

  constructor(protected http: HttpClient) {}

  create(beneficiary: IBeneficiary): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(beneficiary);
    return this.http
      .post<IBeneficiary>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(beneficiary: IBeneficiary): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(beneficiary);
    return this.http
      .put<IBeneficiary>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IBeneficiary>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IBeneficiary[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IBeneficiary[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  protected convertDateFromClient(beneficiary: IBeneficiary): IBeneficiary {
    const copy: IBeneficiary = Object.assign({}, beneficiary, {
      birthDate: beneficiary.birthDate != null && beneficiary.birthDate.isValid() ? beneficiary.birthDate.format(DATE_FORMAT) : null
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.birthDate = res.body.birthDate != null ? moment(res.body.birthDate) : null;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((beneficiary: IBeneficiary) => {
        beneficiary.birthDate = beneficiary.birthDate != null ? moment(beneficiary.birthDate) : null;
      });
    }
    return res;
  }
}
