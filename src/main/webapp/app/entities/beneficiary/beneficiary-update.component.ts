import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { IBeneficiary, Beneficiary } from 'app/shared/model/beneficiary.model';
import { BeneficiaryService } from './beneficiary.service';

@Component({
  selector: 'jhi-beneficiary-update',
  templateUrl: './beneficiary-update.component.html'
})
export class BeneficiaryUpdateComponent implements OnInit {
  isSaving: boolean;
  birthDateDp: any;

  editForm = this.fb.group({
    id: [],
    lastName: [],
    firstName: [],
    email: [],
    phoneNumber: [],
    gender: [],
    birthDate: [],
    Address: [],
    CP: [],
    City: [],
    hasCV: [],
    financialResources: [],
    levelOfStudy: []
  });

  constructor(protected beneficiaryService: BeneficiaryService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ beneficiary }) => {
      this.updateForm(beneficiary);
    });
  }

  updateForm(beneficiary: IBeneficiary) {
    this.editForm.patchValue({
      id: beneficiary.id,
      lastName: beneficiary.lastName,
      firstName: beneficiary.firstName,
      email: beneficiary.email,
      phoneNumber: beneficiary.phoneNumber,
      gender: beneficiary.gender,
      birthDate: beneficiary.birthDate,
      Address: beneficiary.Address,
      CP: beneficiary.CP,
      City: beneficiary.City,
      hasCV: beneficiary.hasCV,
      financialResources: beneficiary.financialResources,
      levelOfStudy: beneficiary.levelOfStudy
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const beneficiary = this.createFromForm();
    if (beneficiary.id !== undefined) {
      this.subscribeToSaveResponse(this.beneficiaryService.update(beneficiary));
    } else {
      this.subscribeToSaveResponse(this.beneficiaryService.create(beneficiary));
    }
  }

  private createFromForm(): IBeneficiary {
    return {
      ...new Beneficiary(),
      id: this.editForm.get(['id']).value,
      lastName: this.editForm.get(['lastName']).value,
      firstName: this.editForm.get(['firstName']).value,
      email: this.editForm.get(['email']).value,
      phoneNumber: this.editForm.get(['phoneNumber']).value,
      gender: this.editForm.get(['gender']).value,
      birthDate: this.editForm.get(['birthDate']).value,
      Address: this.editForm.get(['Address']).value,
      CP: this.editForm.get(['CP']).value,
      City: this.editForm.get(['City']).value,
      hasCV: this.editForm.get(['hasCV']).value,
      financialResources: this.editForm.get(['financialResources']).value,
      levelOfStudy: this.editForm.get(['levelOfStudy']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IBeneficiary>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
}
