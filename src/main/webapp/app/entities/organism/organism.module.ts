import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { LcsSharedModule } from 'app/shared';
import {
  OrganismComponent,
  OrganismDetailComponent,
  OrganismUpdateComponent,
  OrganismDeletePopupComponent,
  OrganismDeleteDialogComponent,
  organismRoute,
  organismPopupRoute
} from './';

const ENTITY_STATES = [...organismRoute, ...organismPopupRoute];

@NgModule({
  imports: [LcsSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    OrganismComponent,
    OrganismDetailComponent,
    OrganismUpdateComponent,
    OrganismDeleteDialogComponent,
    OrganismDeletePopupComponent
  ],
  entryComponents: [OrganismComponent, OrganismUpdateComponent, OrganismDeleteDialogComponent, OrganismDeletePopupComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class LcsOrganismModule {}
