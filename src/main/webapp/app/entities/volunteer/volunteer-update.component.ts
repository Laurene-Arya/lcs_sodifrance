import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { IVolunteer, Volunteer } from 'app/shared/model/volunteer.model';
import { VolunteerService } from './volunteer.service';

@Component({
  selector: 'jhi-volunteer-update',
  templateUrl: './volunteer-update.component.html'
})
export class VolunteerUpdateComponent implements OnInit {
  isSaving: boolean;

  editForm = this.fb.group({
    id: [],
    firstName: [],
    lastName: []
  });

  constructor(protected volunteerService: VolunteerService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ volunteer }) => {
      this.updateForm(volunteer);
    });
  }

  updateForm(volunteer: IVolunteer) {
    this.editForm.patchValue({
      id: volunteer.id,
      firstName: volunteer.firstName,
      lastName: volunteer.lastName
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const volunteer = this.createFromForm();
    if (volunteer.id !== undefined) {
      this.subscribeToSaveResponse(this.volunteerService.update(volunteer));
    } else {
      this.subscribeToSaveResponse(this.volunteerService.create(volunteer));
    }
  }

  private createFromForm(): IVolunteer {
    return {
      ...new Volunteer(),
      id: this.editForm.get(['id']).value,
      firstName: this.editForm.get(['firstName']).value,
      lastName: this.editForm.get(['lastName']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IVolunteer>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
}
