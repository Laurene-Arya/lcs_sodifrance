import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { LcsSharedModule } from 'app/shared';
import {
  VolunteerComponent,
  VolunteerDetailComponent,
  VolunteerUpdateComponent,
  VolunteerDeletePopupComponent,
  VolunteerDeleteDialogComponent,
  volunteerRoute,
  volunteerPopupRoute
} from './';

const ENTITY_STATES = [...volunteerRoute, ...volunteerPopupRoute];

@NgModule({
  imports: [LcsSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    VolunteerComponent,
    VolunteerDetailComponent,
    VolunteerUpdateComponent,
    VolunteerDeleteDialogComponent,
    VolunteerDeletePopupComponent
  ],
  entryComponents: [VolunteerComponent, VolunteerUpdateComponent, VolunteerDeleteDialogComponent, VolunteerDeletePopupComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class LcsVolunteerModule {}
