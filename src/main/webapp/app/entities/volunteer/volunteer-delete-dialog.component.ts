import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IVolunteer } from 'app/shared/model/volunteer.model';
import { VolunteerService } from './volunteer.service';

@Component({
  selector: 'jhi-volunteer-delete-dialog',
  templateUrl: './volunteer-delete-dialog.component.html'
})
export class VolunteerDeleteDialogComponent {
  volunteer: IVolunteer;

  constructor(protected volunteerService: VolunteerService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.volunteerService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'volunteerListModification',
        content: 'Deleted an volunteer'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-volunteer-delete-popup',
  template: ''
})
export class VolunteerDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ volunteer }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(VolunteerDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.volunteer = volunteer;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/volunteer', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/volunteer', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
