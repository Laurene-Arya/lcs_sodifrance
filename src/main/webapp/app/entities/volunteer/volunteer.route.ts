import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Volunteer } from 'app/shared/model/volunteer.model';
import { VolunteerService } from './volunteer.service';
import { VolunteerComponent } from './volunteer.component';
import { VolunteerDetailComponent } from './volunteer-detail.component';
import { VolunteerUpdateComponent } from './volunteer-update.component';
import { VolunteerDeletePopupComponent } from './volunteer-delete-dialog.component';
import { IVolunteer } from 'app/shared/model/volunteer.model';

@Injectable({ providedIn: 'root' })
export class VolunteerResolve implements Resolve<IVolunteer> {
  constructor(private service: VolunteerService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IVolunteer> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<Volunteer>) => response.ok),
        map((volunteer: HttpResponse<Volunteer>) => volunteer.body)
      );
    }
    return of(new Volunteer());
  }
}

export const volunteerRoute: Routes = [
  {
    path: '',
    component: VolunteerComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'Volunteers'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: VolunteerDetailComponent,
    resolve: {
      volunteer: VolunteerResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Volunteers'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: VolunteerUpdateComponent,
    resolve: {
      volunteer: VolunteerResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Volunteers'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: VolunteerUpdateComponent,
    resolve: {
      volunteer: VolunteerResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Volunteers'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const volunteerPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: VolunteerDeletePopupComponent,
    resolve: {
      volunteer: VolunteerResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Volunteers'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
