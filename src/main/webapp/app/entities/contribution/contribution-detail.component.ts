import { filter, map } from 'rxjs/operators';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Volunteer, IVolunteer } from './../../shared/model/volunteer.model';
import { VolunteerService } from './../volunteer/volunteer.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IContribution } from 'app/shared/model/contribution.model';

@Component({
  selector: 'jhi-contribution-detail',
  templateUrl: './contribution-detail.component.html'
})
export class ContributionDetailComponent implements OnInit {
  contribution: IContribution;
  volunteer: IVolunteer;

  constructor(protected activatedRoute: ActivatedRoute, protected volunteerService: VolunteerService) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ contribution }) => {
      this.contribution = contribution;
      this.volunteerService
        .find(contribution.volunteerId)
        .pipe(
          filter((subResMayBeOk: HttpResponse<IVolunteer>) => subResMayBeOk.ok),
          map((subResponse: HttpResponse<IVolunteer>) => subResponse.body)
        )
        .subscribe((subRes: IVolunteer) => (this.volunteer = subRes));
    });
  }

  previousState() {
    window.history.back();
  }
}
