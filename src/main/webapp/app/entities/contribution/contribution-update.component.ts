import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiAlertService } from 'ng-jhipster';
import { IContribution, Contribution } from 'app/shared/model/contribution.model';
import { ContributionService } from './contribution.service';
import { IVolunteer } from 'app/shared/model/volunteer.model';
import { VolunteerService } from 'app/entities/volunteer';

@Component({
  selector: 'jhi-contribution-update',
  templateUrl: './contribution-update.component.html'
})
export class ContributionUpdateComponent implements OnInit {
  isSaving: boolean;

  volunteers: IVolunteer[];

  editForm = this.fb.group({
    id: [],
    date: [],
    hours: [],
    total: [],
    totalCharged: [],
    volunteerId: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected contributionService: ContributionService,
    protected volunteerService: VolunteerService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ contribution }) => {
      this.updateForm(contribution);
    });
    this.volunteerService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IVolunteer[]>) => mayBeOk.ok),
        map((response: HttpResponse<IVolunteer[]>) => response.body)
      )
      .subscribe((res: IVolunteer[]) => (this.volunteers = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.volunteerService
      .query({ filter: 'contribution-is-null' })
      .pipe(
        filter((mayBeOk: HttpResponse<IVolunteer[]>) => mayBeOk.ok),
        map((response: HttpResponse<IVolunteer[]>) => response.body)
      )
      .subscribe(
        (res: IVolunteer[]) => {
          if (!this.editForm.get('volunteerId').value) {
            this.volunteers = res;
          } else {
            this.volunteerService
              .find(this.editForm.get('volunteerId').value)
              .pipe(
                filter((subResMayBeOk: HttpResponse<IVolunteer>) => subResMayBeOk.ok),
                map((subResponse: HttpResponse<IVolunteer>) => subResponse.body)
              )
              .subscribe(
                (subRes: IVolunteer) => (this.volunteers = [subRes].concat(res)),
                (subRes: HttpErrorResponse) => this.onError(subRes.message)
              );
          }
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  updateForm(contribution: IContribution) {
    this.editForm.patchValue({
      id: contribution.id,
      date: contribution.date != null ? contribution.date.format(DATE_TIME_FORMAT) : null,
      hours: contribution.hours,
      total: contribution.total,
      totalCharged: contribution.totalCharged,
      volunteerId: contribution.volunteerId
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const contribution = this.createFromForm();
    if (contribution.id !== undefined) {
      this.subscribeToSaveResponse(this.contributionService.update(contribution));
    } else {
      this.subscribeToSaveResponse(this.contributionService.create(contribution));
    }
  }

  private createFromForm(): IContribution {
    return {
      ...new Contribution(),
      id: this.editForm.get(['id']).value,
      date: this.editForm.get(['date']).value != null ? moment(this.editForm.get(['date']).value, DATE_TIME_FORMAT) : undefined,
      hours: this.editForm.get(['hours']).value,
      total: this.editForm.get(['total']).value,
      totalCharged: this.editForm.get(['totalCharged']).value,
      volunteerId: this.editForm.get(['volunteerId']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IContribution>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackVolunteerById(index: number, item: IVolunteer) {
    return item.id;
  }
}
