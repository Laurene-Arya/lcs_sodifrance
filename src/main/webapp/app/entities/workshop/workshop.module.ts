import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { LcsSharedModule } from 'app/shared';
import {
  WorkshopComponent,
  WorkshopDetailComponent,
  WorkshopUpdateComponent,
  WorkshopDeletePopupComponent,
  WorkshopDeleteDialogComponent,
  workshopRoute,
  workshopPopupRoute
} from './';

const ENTITY_STATES = [...workshopRoute, ...workshopPopupRoute];

@NgModule({
  imports: [LcsSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    WorkshopComponent,
    WorkshopDetailComponent,
    WorkshopUpdateComponent,
    WorkshopDeleteDialogComponent,
    WorkshopDeletePopupComponent
  ],
  entryComponents: [WorkshopComponent, WorkshopUpdateComponent, WorkshopDeleteDialogComponent, WorkshopDeletePopupComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class LcsWorkshopModule {}
