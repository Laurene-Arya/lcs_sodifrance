import { IOrganism } from 'app/shared/model/organism.model';
import { HttpResponse } from '@angular/common/http';
import { filter, map } from 'rxjs/operators';
import { VolunteerService } from 'app/entities/volunteer';
import { IVolunteer } from 'app/shared/model/volunteer.model';
import { IBeneficiary } from 'app/shared/model/beneficiary.model';
import { OrganismService } from 'app/entities/organism';
import { BeneficiaryService } from 'app/entities/beneficiary';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IWorkshop } from 'app/shared/model/workshop.model';

@Component({
  selector: 'jhi-workshop-detail',
  templateUrl: './workshop-detail.component.html'
})
export class WorkshopDetailComponent implements OnInit {
  workshop: IWorkshop;
  volunteer: IVolunteer;
  beneficiary: IBeneficiary;
  organism: IOrganism;

  constructor(
    protected activatedRoute: ActivatedRoute,
    protected volunteerService: VolunteerService,
    protected beneficirayService: BeneficiaryService,
    protected organismService: OrganismService
  ) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ workshop }) => {
      this.workshop = workshop;
      this.volunteerService
        .find(workshop.volunteerId)
        .pipe(
          filter((subResMayBeOk: HttpResponse<IVolunteer>) => subResMayBeOk.ok),
          map((subResponse: HttpResponse<IVolunteer>) => subResponse.body)
        )
        .subscribe((subRes: IVolunteer) => (this.volunteer = subRes));

      this.beneficirayService
        .find(workshop.beneficiarId)
        .pipe(
          filter((subResMayBeOk: HttpResponse<IBeneficiary>) => subResMayBeOk.ok),
          map((subResponse: HttpResponse<IBeneficiary>) => subResponse.body)
        )
        .subscribe((subRes: IBeneficiary) => (this.beneficiary = subRes));

      this.organismService
        .find(workshop.organismId)
        .pipe(
          filter((subResMayBeOk: HttpResponse<IOrganism>) => subResMayBeOk.ok),
          map((subResponse: HttpResponse<IOrganism>) => subResponse.body)
        )
        .subscribe((subRes: IOrganism) => (this.organism = subRes));
    });
  }

  previousState() {
    window.history.back();
  }
}
