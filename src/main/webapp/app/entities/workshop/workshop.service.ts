import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IWorkshop } from 'app/shared/model/workshop.model';

type EntityResponseType = HttpResponse<IWorkshop>;
type EntityArrayResponseType = HttpResponse<IWorkshop[]>;

@Injectable({ providedIn: 'root' })
export class WorkshopService {
  public resourceUrl = SERVER_API_URL + 'api/workshops';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/workshops';

  constructor(protected http: HttpClient) {}

  create(workshop: IWorkshop): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(workshop);
    return this.http
      .post<IWorkshop>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(workshop: IWorkshop): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(workshop);
    return this.http
      .put<IWorkshop>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IWorkshop>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IWorkshop[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IWorkshop[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  protected convertDateFromClient(workshop: IWorkshop): IWorkshop {
    const copy: IWorkshop = Object.assign({}, workshop, {
      date: workshop.date != null && workshop.date.isValid() ? workshop.date.format(DATE_FORMAT) : null
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.date = res.body.date != null ? moment(res.body.date) : null;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((workshop: IWorkshop) => {
        workshop.date = workshop.date != null ? moment(workshop.date) : null;
      });
    }
    return res;
  }
}
