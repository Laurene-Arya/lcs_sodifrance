import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { JhiAlertService } from 'ng-jhipster';
import { IWorkshop, Workshop } from 'app/shared/model/workshop.model';
import { WorkshopService } from './workshop.service';
import { IBeneficiary } from 'app/shared/model/beneficiary.model';
import { BeneficiaryService } from 'app/entities/beneficiary';
import { IOrganism } from 'app/shared/model/organism.model';
import { OrganismService } from 'app/entities/organism';
import { IVolunteer } from 'app/shared/model/volunteer.model';
import { VolunteerService } from 'app/entities/volunteer';

@Component({
  selector: 'jhi-workshop-update',
  templateUrl: './workshop-update.component.html'
})
export class WorkshopUpdateComponent implements OnInit {
  isSaving: boolean;

  beneficiaries: IBeneficiary[];

  organisms: IOrganism[];

  volunteers: IVolunteer[];
  dateDp: any;
  periode: any;

  editForm = this.fb.group({
    id: [],
    date: [],
    periode: [],
    beneficiaryId: [],
    organismId: [],
    volunteerId: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected workshopService: WorkshopService,
    protected beneficiaryService: BeneficiaryService,
    protected organismService: OrganismService,
    protected volunteerService: VolunteerService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ workshop }) => {
      this.updateForm(workshop);
    });
    this.beneficiaryService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IBeneficiary[]>) => mayBeOk.ok),
        map((response: HttpResponse<IBeneficiary[]>) => response.body)
      )
      .subscribe((res: IBeneficiary[]) => (this.beneficiaries = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.beneficiaryService
      .query({ filter: 'workshop-is-null' })
      .pipe(
        filter((mayBeOk: HttpResponse<IBeneficiary[]>) => mayBeOk.ok),
        map((response: HttpResponse<IBeneficiary[]>) => response.body)
      )
      .subscribe(
        (res: IBeneficiary[]) => {
          if (!this.editForm.get('beneficiaryId').value) {
            this.beneficiaries = res;
          } else {
            this.beneficiaryService
              .find(this.editForm.get('beneficiaryId').value)
              .pipe(
                filter((subResMayBeOk: HttpResponse<IBeneficiary>) => subResMayBeOk.ok),
                map((subResponse: HttpResponse<IBeneficiary>) => subResponse.body)
              )
              .subscribe(
                (subRes: IBeneficiary) => (this.beneficiaries = [subRes].concat(res)),
                (subRes: HttpErrorResponse) => this.onError(subRes.message)
              );
          }
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
    this.organismService
      .query({ filter: 'workshop-is-null' })
      .pipe(
        filter((mayBeOk: HttpResponse<IOrganism[]>) => mayBeOk.ok),
        map((response: HttpResponse<IOrganism[]>) => response.body)
      )
      .subscribe(
        (res: IOrganism[]) => {
          if (!this.editForm.get('organismId').value) {
            this.organisms = res;
          } else {
            this.organismService
              .find(this.editForm.get('organismId').value)
              .pipe(
                filter((subResMayBeOk: HttpResponse<IOrganism>) => subResMayBeOk.ok),
                map((subResponse: HttpResponse<IOrganism>) => subResponse.body)
              )
              .subscribe(
                (subRes: IOrganism) => (this.organisms = [subRes].concat(res)),
                (subRes: HttpErrorResponse) => this.onError(subRes.message)
              );
          }
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
    this.volunteerService
      .query({ filter: 'workshop-is-null' })
      .pipe(
        filter((mayBeOk: HttpResponse<IVolunteer[]>) => mayBeOk.ok),
        map((response: HttpResponse<IVolunteer[]>) => response.body)
      )
      .subscribe(
        (res: IVolunteer[]) => {
          if (!this.editForm.get('volunteerId').value) {
            this.volunteers = res;
          } else {
            this.volunteerService
              .find(this.editForm.get('volunteerId').value)
              .pipe(
                filter((subResMayBeOk: HttpResponse<IVolunteer>) => subResMayBeOk.ok),
                map((subResponse: HttpResponse<IVolunteer>) => subResponse.body)
              )
              .subscribe(
                (subRes: IVolunteer) => (this.volunteers = [subRes].concat(res)),
                (subRes: HttpErrorResponse) => this.onError(subRes.message)
              );
          }
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
    this.volunteerService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IVolunteer[]>) => mayBeOk.ok),
        map((response: HttpResponse<IVolunteer[]>) => response.body)
      )
      .subscribe((res: IVolunteer[]) => (this.volunteers = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(workshop: IWorkshop) {
    this.editForm.patchValue({
      id: workshop.id,
      date: workshop.date,
      periode: workshop.periode,
      beneficiaryId: workshop.beneficiaryId,
      organismId: workshop.organismId,
      volunteerId: workshop.volunteerId
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const workshop = this.createFromForm();
    if (workshop.id !== undefined) {
      this.subscribeToSaveResponse(this.workshopService.update(workshop));
    } else {
      this.subscribeToSaveResponse(this.workshopService.create(workshop));
    }
  }

  private createFromForm(): IWorkshop {
    return {
      ...new Workshop(),
      id: this.editForm.get(['id']).value,
      date: this.editForm.get(['date']).value,
      periode: this.editForm.get(['periode']).value,
      beneficiaryId: this.editForm.get(['beneficiaryId']).value,
      organismId: this.editForm.get(['organismId']).value,
      volunteerId: this.editForm.get(['volunteerId']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IWorkshop>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackBeneficiaryById(index: number, item: IBeneficiary) {
    return item.id;
  }

  trackOrganismById(index: number, item: IOrganism) {
    return item.id;
  }

  trackVolunteerById(index: number, item: IVolunteer) {
    return item.id;
  }
}
