import { OrganismService } from 'app/entities/organism';
import { BeneficiaryService } from 'app/entities/beneficiary';
import { VolunteerService } from 'app/entities/volunteer';
import { IOrganism } from 'app/shared/model/organism.model';
import { IVolunteer } from 'app/shared/model/volunteer.model';
import { IBeneficiary } from 'app/shared/model/beneficiary.model';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import { IWorkshop } from 'app/shared/model/workshop.model';
import { AccountService } from 'app/core';

import { ITEMS_PER_PAGE } from 'app/shared';
import { WorkshopService } from './workshop.service';

@Component({
  selector: 'jhi-workshop',
  templateUrl: './workshop.component.html'
})
export class WorkshopComponent implements OnInit, OnDestroy {
  currentAccount: any;
  workshops: IWorkshop[];
  volunteers: IVolunteer[];
  beneficiaries: IBeneficiary[];
  organisms: IOrganism[];
  error: any;
  success: any;
  eventSubscriber: Subscription;
  currentSearch: string;
  routeData: any;
  links: any;
  totalItems: any;
  itemsPerPage: any;
  page: any;
  predicate: any;
  previousPage: any;
  reverse: any;

  constructor(
    protected workshopService: WorkshopService,
    protected volunteerService: VolunteerService,
    protected beneficiaryService: BeneficiaryService,
    protected organismService: OrganismService,
    protected parseLinks: JhiParseLinks,
    protected jhiAlertService: JhiAlertService,
    protected accountService: AccountService,
    protected activatedRoute: ActivatedRoute,
    protected router: Router,
    protected eventManager: JhiEventManager
  ) {
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.routeData = this.activatedRoute.data.subscribe(data => {
      this.page = data.pagingParams.page;
      this.previousPage = data.pagingParams.page;
      this.reverse = data.pagingParams.ascending;
      this.predicate = data.pagingParams.predicate;
    });
    this.currentSearch =
      this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['search'] ? this.activatedRoute.snapshot.params['search'] : '';
  }

  loadAll() {
    if (this.currentSearch) {
      this.workshopService
        .search({
          page: this.page - 1,
          query: this.currentSearch,
          size: this.itemsPerPage,
          sort: this.sort()
        })
        .subscribe(
          (res: HttpResponse<IWorkshop[]>) => this.paginateWorkshops(res.body, res.headers),
          (res: HttpErrorResponse) => this.onError(res.message)
        );
      return;
    }
    this.workshopService
      .query({
        page: this.page - 1,
        size: this.itemsPerPage,
        sort: this.sort()
      })
      .subscribe(
        (res: HttpResponse<IWorkshop[]>) => this.paginateWorkshops(res.body, res.headers),
        (res: HttpErrorResponse) => this.onError(res.message)
      );

    this.volunteerService
      .query()
      .pipe(
        filter((subResMayBeOk: HttpResponse<IVolunteer[]>) => subResMayBeOk.ok),
        map((subResponse: HttpResponse<IVolunteer[]>) => subResponse.body)
      )
      .subscribe((subRes: IVolunteer[]) => (this.volunteers = subRes));

    this.beneficiaryService
      .query()
      .pipe(
        filter((subResMayBeOk: HttpResponse<IBeneficiary[]>) => subResMayBeOk.ok),
        map((subResponse: HttpResponse<IBeneficiary[]>) => subResponse.body)
      )
      .subscribe((subRes: IBeneficiary[]) => (this.beneficiaries = subRes));

    this.organismService
      .query()
      .pipe(
        filter((subResMayBeOk: HttpResponse<IOrganism[]>) => subResMayBeOk.ok),
        map((subResponse: HttpResponse<IOrganism[]>) => subResponse.body)
      )
      .subscribe((subRes: IOrganism[]) => (this.organisms = subRes));
  }

  loadPage(page: number) {
    if (page !== this.previousPage) {
      this.previousPage = page;
      this.transition();
    }
  }

  transition() {
    this.router.navigate(['/workshop'], {
      queryParams: {
        page: this.page,
        size: this.itemsPerPage,
        search: this.currentSearch,
        sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
      }
    });
    this.loadAll();
  }

  clear() {
    this.page = 0;
    this.currentSearch = '';
    this.router.navigate([
      '/workshop',
      {
        page: this.page,
        sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
      }
    ]);
    this.loadAll();
  }

  search(query) {
    if (!query) {
      return this.clear();
    }
    this.page = 0;
    this.currentSearch = query;
    this.router.navigate([
      '/workshop',
      {
        search: this.currentSearch,
        page: this.page,
        sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
      }
    ]);
    this.loadAll();
  }

  ngOnInit() {
    this.loadAll();
    this.accountService.identity().then(account => {
      this.currentAccount = account;
    });
    this.registerChangeInWorkshops();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: IWorkshop) {
    return item.id;
  }

  registerChangeInWorkshops() {
    this.eventSubscriber = this.eventManager.subscribe('workshopListModification', response => this.loadAll());
  }

  sort() {
    const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected paginateWorkshops(data: IWorkshop[], headers: HttpHeaders) {
    this.links = this.parseLinks.parse(headers.get('link'));
    this.totalItems = parseInt(headers.get('X-Total-Count'), 10);
    this.workshops = data;
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  getVolunteerNames(volunteerId: number): String {
    const aVolunteer: IVolunteer = this.volunteers.find((v: IVolunteer) => {
      return v.id === volunteerId;
    });
    return aVolunteer.lastName + ' ' + aVolunteer.firstName;
  }

  getBeneficiaryNames(beneficiaryId: number): String {
    const aBeneficiary: IBeneficiary = this.beneficiaries.find((v: IBeneficiary) => {
      return v.id === beneficiaryId;
    });
    return aBeneficiary.lastName + ' ' + aBeneficiary.firstName;
  }

  getOrganismLibelle(organismId: number): String {
    const aOrganism: IOrganism = this.organisms.find((o: IOrganism) => {
      return o.id === organismId;
    });
    return aOrganism.name;
  }
}
