import { NgModule } from '@angular/core';

import { LcsSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent } from './';

@NgModule({
  imports: [LcsSharedLibsModule],
  declarations: [JhiAlertComponent, JhiAlertErrorComponent],
  exports: [LcsSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent]
})
export class LcsSharedCommonModule {}
