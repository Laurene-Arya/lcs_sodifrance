export interface ISocialAid {
  id?: number;
  libelle?: string;
}

export class SocialAid implements ISocialAid {
  constructor(public id?: number, public libelle?: string) {}
}
