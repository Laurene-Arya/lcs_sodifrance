import { Moment } from 'moment';

export interface IWorkshop {
  id?: number;
  date?: Moment;
  periode?: string;
  beneficiaryId?: number;
  organismId?: number;
  volunteerId?: number;
}

export class Workshop implements IWorkshop {
  constructor(
    public id?: number,
    public date?: Moment,
    public periode?: string,
    public beneficiaryId?: number,
    public organismId?: number,
    public volunteerId?: number
  ) {}
}
