import { Moment } from 'moment';
import { IWorkshop } from 'app/shared/model/workshop.model';
import { ISocialAid } from 'app/shared/model/social-aid.model';

export interface IBeneficiary {
  id?: number;
  lastName?: string;
  firstName?: string;
  email?: string;
  phoneNumber?: string;
  gender?: string;
  birthDate?: Moment;
  Address?: string;
  CP?: number;
  City?: string;
  hasCV?: boolean;
  financialResources?: string;
  levelOfStudy?: string;
  workshops?: IWorkshop[];
  socialAids?: ISocialAid[];
}

export class Beneficiary implements IBeneficiary {
  constructor(
    public id?: number,
    public lastName?: string,
    public firstName?: string,
    public email?: string,
    public phoneNumber?: string,
    public gender?: string,
    public birthDate?: Moment,
    public Address?: string,
    public CP?: number,
    public City?: string,
    public hasCV?: boolean,
    public financialResources?: string,
    public levelOfStudy?: string,
    public workshops?: IWorkshop[],
    public socialAids?: ISocialAid[]
  ) {
    this.hasCV = this.hasCV || false;
  }
}
