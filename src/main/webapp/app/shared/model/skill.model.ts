export interface ISkill {
  id?: number;
  libelle?: string;
  hourlyRate?: number;
}

export class Skill implements ISkill {
  constructor(public id?: number, public libelle?: string, public hourlyRate?: number) {}
}
