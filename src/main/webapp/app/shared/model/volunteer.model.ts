import { IWorkshop } from 'app/shared/model/workshop.model';
import { ISkill } from 'app/shared/model/skill.model';
import { IContribution } from 'app/shared/model/contribution.model';

export interface IVolunteer {
  id?: number;
  firstName?: string;
  lastName?: string;
  workshops?: IWorkshop[];
  skills?: ISkill[];
  constributions?: IContribution[];
}

export class Volunteer implements IVolunteer {
  constructor(
    public id?: number,
    public firstName?: string,
    public lastName?: string,
    public workshops?: IWorkshop[],
    public skills?: ISkill[],
    public constributions?: IContribution[]
  ) {}
}
