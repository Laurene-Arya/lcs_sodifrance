export interface IOrganism {
  id?: number;
  name?: string;
  type?: string;
  address?: string;
  contactName?: string;
  contactEmail?: string;
  contactPhone?: string;
  convention?: boolean;
}

export class Organism implements IOrganism {
  constructor(
    public id?: number,
    public name?: string,
    public type?: string,
    public address?: string,
    public contactName?: string,
    public contactEmail?: string,
    public contactPhone?: string,
    public convention?: boolean
  ) {
    this.convention = this.convention || false;
  }
}
