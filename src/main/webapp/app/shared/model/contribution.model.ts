import { Moment } from 'moment';

export interface IContribution {
  id?: number;
  date?: Moment;
  hours?: number;
  total?: string;
  totalCharged?: number;
  volunteerId?: number;
}

export class Contribution implements IContribution {
  constructor(
    public id?: number,
    public date?: Moment,
    public hours?: number,
    public total?: string,
    public totalCharged?: number,
    public volunteerId?: number
  ) {}
}
