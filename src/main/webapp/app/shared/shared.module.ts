import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { LcsSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective } from './';

@NgModule({
  imports: [LcsSharedCommonModule],
  declarations: [JhiLoginModalComponent, HasAnyAuthorityDirective],
  entryComponents: [JhiLoginModalComponent],
  exports: [LcsSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class LcsSharedModule {
  static forRoot() {
    return {
      ngModule: LcsSharedModule
    };
  }
}
